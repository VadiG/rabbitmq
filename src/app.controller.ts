import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import {
  Ctx,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { EventsGateway } from './events.gateway';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly eventsGateway: EventsGateway,
  ) {}

  @MessagePattern('notifications')
  getNotifications(@Payload() data: string, @Ctx() context: RmqContext) {
    // {
    //   "pattern": "notifications",
    //     "data": "test data"
    // }
    Logger.log('Received new message');
    this.eventsGateway.sendMessage('notifications_event', data);
  }
}
