## Собрать образ с приложением
```
docker-compose -f docker-compose.yml build
```

## Запустить проект
```
docker-compose -f docker-compose.yml up
```

### Сообщения принимаются в формате
где [notifications] - паттерн для обработки сообщения, [data] - полезная нагрузка
```
{
   "pattern": [notifications],
   "data": [data]
}
```